# Spring boot 2 & Angular 6

This is my first sample project service oriented with RESTful.
## Installation
#### Prerequisites
+ Java 8 

+ Gradle 5.6+

+ Node v12.11.1+

+ npm 6.12.1+

+ Angular CLI: 6.2.9+

Use the package manager [npm](https://www.npmjs.com/get-npm) to install node_modules.



```bash
npm install
```

Install Angular CLI
```bash
npm i @angular/cli
```

Open Spring boot application.properties and configure it with your database username, password and hibernate dialect, don't forget to change the datasource url 

```bash
spring.datasource.url= #database url
spring.datasource.username= #username
spring.datasource.password= #password
spring.jpa.properties.hibernate.dialect = #database dialect
```

## Techonology Stack
| Component | Techonology |
| ------------- | ------------- |
| Backend(REST)  | Spring boot 2(Java)  |
| Frontend  | Angular 6  |
| Security | JWT Authorization |
| Database | PostgreSQL |
| Persistance | JPA |
| Server Build Tools | Gradle |
| Client Build Tools | angular-cli, npm install|


## API Doc

### User Rest 
###### no permissions needed

| Request | URL | Functionality |
| ------------- | ------------- | ------------- |
|![](https://img.shields.io/badge/POST-blue) | /register | Creates new user
|![](https://img.shields.io/badge/POST-blue) | /login | Generate JWT token


###### must be authenticated

| Request | URL | Functionality |
| ------------- | ------------- | ------------- |
|![](https://img.shields.io/badge/GET-green) | /profile | Loads current user information(needs to be logged in)
|![](https://img.shields.io/badge/GET-green) | /getByName | Returns list of users with given name

### Article Rest 

###### must be authenticated
| Request | URL | Functionality |
| ------------- | ------------- | ------------- |
|![](https://img.shields.io/badge/POST-blue) | /article/create | Creates new article
|![](https://img.shields.io/badge/PUT-orange) | /article/edit/{id} | Updates  article 
|![](https://img.shields.io/badge/DELETE-red) | /article/{id} | Deletes  article 
|![](https://img.shields.io/badge/GET-green) | /article/articleById | Search for article by id
|![](https://img.shields.io/badge/GET-green) | /article/byAuthor | Returns list of articles created by the current user 

### Admin Rest 

###### must be authenticated with role 'ADMIN'
| Request | URL | Functionality |
| ------------- | ------------- | ------------- |
|![](https://img.shields.io/badge/GET-green) | /admin/users | List all users
|![](https://img.shields.io/badge/DELETE-red) | /admin/users/{id} | Delete user
|![](https://img.shields.io/badge/PUT-orange) | /admin/users/{id} | Update user 
|![](https://img.shields.io/badge/GET-green) | /admin/userByEmail | Search for user by email
|![](https://img.shields.io/badge/GET-green) | /admin/userById | Search for user by id
|![](https://img.shields.io/badge/GET-green) | /admin/listAll | Returns all articles


## Usage

After installation of node_modules and anglur-cli

Backend run:
```bash
go to project folder -> cd backend 
./gradlew clean build
java -jar build/libs/demo-v1.jar
```

Frontend start 
```bash
go to project folder -> cd frontend
	
ng serve
```

> Login credentials 
```bash
        USER
username: user@email.com 
password: 1234
```

```bash
        ADMIN
username: admin@email.com
password: 1234
```

## Bugs

