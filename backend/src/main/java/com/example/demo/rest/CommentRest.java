package com.example.demo.rest;

import com.example.demo.domain.Article;
import com.example.demo.domain.Comment;
import com.example.demo.domain.User;
import com.example.demo.dto.CommentSaveDto;
import com.example.demo.response.ApiResponse;
import com.example.demo.service.ArticleService;
import com.example.demo.service.CommentService;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@PreAuthorize("isAuthenticated()")
public class CommentRest {

    public static final Logger logger = LoggerFactory.getLogger(AdminRest.class);

    @Autowired
    private CommentService commentService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private UserService userService;

    @GetMapping("/article/{id}/comments")
    public List<Comment> getAllCommentsByPostId(@PathVariable(value = "id") Long id){
        List<Comment> comments = commentService.findByArticleId(id);
        return comments;
    }

    @PostMapping(value = "/article/{id}/comments")
    public ResponseEntity<Comment> createComment(@PathVariable(value = "id") Long id,@RequestBody CommentSaveDto commentDto){
        Article article = articleService.getById(id);
        Comment comment = new Comment();

        UserDetails loggedUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getByEmail(loggedUser.getUsername());
        comment.setText(commentDto.getText());
        comment.setArticle(article);
        comment.setUserId(user);
        logger.info("Added comment to article-id: {} with text: {}", article.getId(), commentDto.getText());
        commentService.save(comment);

        return new ResponseEntity<>(comment, HttpStatus.OK);
    }

    @DeleteMapping(value = "/article/{articleId}/comments/{id}")
    public ResponseEntity<?> deleteComment(@PathVariable(value = "articleId") Long articleId, @PathVariable(value = "id") Long commentId) {
        Comment comment = commentService.getCommentById(commentId);

        commentService.delete(comment);

        return new ResponseEntity<>(new ApiResponse(true, "Comment deleted!"), HttpStatus.OK);
    }
}
