package com.example.demo.rest;

import com.example.demo.domain.Article;
import com.example.demo.domain.Category;
import com.example.demo.dto.ArticleDto;
import com.example.demo.domain.User;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.response.ApiResponse;
import com.example.demo.service.ArticleService;
import com.example.demo.service.CategoryService;
import com.example.demo.service.UserService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ArticleRest {

    public static final Logger logger = LoggerFactory.getLogger(ArticleRest.class);

    @Autowired
    private ArticleService articleService;

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping(value = "/article")
    public ResponseEntity<List<Article>> getAllArticles(){
        List<Article> articles = articleService.listAll();

        return new ResponseEntity<>(articles, HttpStatus.OK);
    }

    @PostMapping(value = "/article", consumes = "application/json")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> createArticle(@RequestBody ArticleDto articleDto){
        Article article = new Article();

        UserDetails user = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();

        System.out.println(articleDto.getCategory());
        User userAuthor = this.userService.getByEmail(user.getUsername());
        Category category = this.categoryService.getByName(articleDto.getCategory().getCategoryName());

        article.setId(articleDto.getId());
        article.setTitle(articleDto.getTitle());
        article.setContent(articleDto.getContent());
        article.setAuthor(userAuthor);
        article.setCategory(category);

        articleService.save(article);

        return new ResponseEntity<>(articleDto, HttpStatus.OK);
    }

    @PutMapping(value = "/article/edit/{id}", consumes = "application/json")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> editArticle(@PathVariable("id") Long id,@RequestBody ArticleDto article){

        Article updateArticle = articleService.getById(id);

        updateArticle.setTitle(article.getTitle());
        updateArticle.setContent(article.getContent());

        logger.info("Article with id:{} edited by: {}", updateArticle.getId(), updateArticle.getAuthor().getEmail());

        articleService.save(updateArticle);

        return new ResponseEntity<>(new ApiResponse(true, "Update successful"), HttpStatus.OK);
    }

    @DeleteMapping(value = "/article/{id}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> deleteArticle(@PathVariable("id") Long id){
        Article article = articleService.getById(id);

        articleService.delete(article);

        logger.info("Article with id: {} has been deleted!", id);

        return new ResponseEntity<>(new ApiResponse(true, "Article deleted successfully!"), HttpStatus.OK);
    }

    @GetMapping(value = "/article/{id}", produces = "application/json")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Article> articleById(@PathVariable Long id){

        Article article = articleService.getById(id);

        return new ResponseEntity<>(article, HttpStatus.OK);
    }

    @GetMapping("/article/findArticlesByAuthor")
    @PreAuthorize("isAuthenticated()")
    public List<Article> getByAuthor(Long author){
        System.out.println(author);
        return articleService.getByAuthor(author);
    }
}
