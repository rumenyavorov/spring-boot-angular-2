package com.example.demo.rest;

import com.example.demo.domain.Category;
import com.example.demo.dto.CategoryDto;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.response.ApiResponse;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CategoryRest {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/category")
    public ResponseEntity<?> getCategories(){
        List<Category> categories = categoryService.getCategories();

        return ResponseEntity.ok(categories);
    }

    @PostMapping("/category")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> addCategory(@RequestBody CategoryDto categoryDto){
        if(categoryService.existsByName(categoryDto.getCategoryName())){
            return new ResponseEntity<>(new ApiResponse(false, "Category already exists!"),
                    HttpStatus.NOT_ACCEPTABLE);
        }
        Category category = new Category();

        category.setId(categoryDto.getId());
        category.setCategoryName(categoryDto.getCategoryName());

        Category result = categoryRepository.save(category);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/category/{id}")
                .buildAndExpand(result.getCategoryName()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "Category created successfully."));
    }

    @GetMapping(value = "/categoryById", produces = "application/json")
    public ResponseEntity<Category> categoryById(Long id){

        Category category = categoryService.getById(id);

        return new ResponseEntity<>(category, HttpStatus.OK);
    }
}
