package com.example.demo.rest;

import com.example.demo.domain.Article;
import com.example.demo.domain.User;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
@PreAuthorize("hasRole('ADMIN')")
@CrossOrigin(origins = "http://localhost:4200")
public class AdminRest {

    public static final Logger logger = LoggerFactory.getLogger(AdminRest.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ArticleService articleService;

    @GetMapping(value = "/users", produces = "application/json")
    public List<User> getUsers(){
        return userService.getUsers();
    }

    @DeleteMapping(value = "/users/{id}")
    public ResponseEntity<User> delUser(@PathVariable Long id){
        logger.info("User deleted with id: " + id);

        User user = userService.getById(id);
        userService.delete(user);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping(value = "/users/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestBody User user){

        User updateUser = userService.getById(id);;

        updateUser.setName(user.getName());
        updateUser.setEmail(user.getEmail());

        userService.save(updateUser);
        logger.info("User updated with id: "  + user.getUsername());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value = "/userByEmail", produces = "application/json")
    public User userByEmail(String email){
        return this.userService.getByEmail(email);
    }

    @GetMapping(value = "/user/{id}", produces = "application/json")
    public ResponseEntity<User> userById(@PathVariable Long id){
        User user = userService.getById(id);
        return ResponseEntity.ok().body(user);
    }

    @GetMapping("/getByName")
    public List<User> userList(String name) {
        return userService.getByName(name);
    }

    @GetMapping(value = "/listAll", produces = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    public List<Article> listAll(){
        return articleService.listAll();
    }
}
