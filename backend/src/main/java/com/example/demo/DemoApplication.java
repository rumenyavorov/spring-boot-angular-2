package com.example.demo;

import com.example.demo.domain.Article;
import com.example.demo.domain.Category;
import com.example.demo.domain.Role;
import com.example.demo.domain.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.service.ArticleService;
import com.example.demo.service.CategoryService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private CategoryService categoryService;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        roleRepository.save(new Role(1, "USER"));
        roleRepository.save(new Role(2, "ADMIN"));

        categoryService.save(new Category("Development"));
        categoryService.save(new Category("News"));
        categoryService.save(new Category("Design"));

        userService.save(new User("user@email.com", "First User", bCryptPasswordEncoder.encode("1234"), roleRepository.findByRoleName("USER")));
        userService.save(new User("admin@email.com", "First Admin", bCryptPasswordEncoder.encode("1234"), roleRepository.findByRoleName("ADMIN")));

        articleService.save(new Article("First Article By User", "Article1 created by user!", userService.getByEmail("user@email.com"), categoryService.getByName("Development")));
        articleService.save(new Article("Second Article By User", "Article2 created by user!", userService.getByEmail("user@email.com"), categoryService.getByName("News")));
        articleService.save(new Article("Third Article By User", "Article3 created by user!", userService.getByEmail("user@email.com"), categoryService.getByName("Design")));

        articleService.save(new Article("First Article By Admin", "Article1 created by admin!", userService.getByEmail("admin@email.com"), categoryService.getByName("Development")));
        articleService.save(new Article("Second Article By Admin", "Article2 created by admin!", userService.getByEmail("admin@email.com"), categoryService.getByName("News")));
        articleService.save(new Article("Third Article By Admin", "Article3 created by admin!", userService.getByEmail("admin@email.com"), categoryService.getByName("Design")));
    }
}
