package com.example.demo.service.impl;

import com.example.demo.domain.Article;
import com.example.demo.domain.Comment;
import com.example.demo.rest.AdminRest;
import com.example.demo.service.CommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class CommentServiceImpl implements CommentService {

    public static final Logger logger = LoggerFactory.getLogger(AdminRest.class);

    @Autowired
    private EntityManager em;

    @Override
    public List<Comment> findByArticleId(Long id) {

        List<Comment> comments = new ArrayList<>();
        try {
            Query q = em.createQuery("select c from Comment c where c.article.id= :id");
            q.setParameter("id", id);

            comments = q.getResultList();
        } catch (NullPointerException e) {
            logger.error("Article doesn't exist!");
        }


        return comments;
    }

    @Override
    public Comment getCommentById(Long id) {
        Query q = em.createQuery("select c from Comment c where c.id= :id");
        q.setParameter("id", id);

        Comment comment = (Comment) q.getSingleResult();

        if(comment == null) {
            comment = new Comment();
        }

        return comment;
    }

    @Override
    @Transactional
    public void save(Comment comment) {
        em.persist(comment);
        em.flush();
    }

    @Override
    @Transactional
    public void delete(Comment comment) {
        em.remove(comment);
    }
}
