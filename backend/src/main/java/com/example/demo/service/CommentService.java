package com.example.demo.service;

import com.example.demo.domain.Comment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

public interface CommentService {
    List<Comment> findByArticleId(Long id);

    Comment getCommentById(Long id);

    void save(Comment comment);
    void delete(Comment comment);
}
