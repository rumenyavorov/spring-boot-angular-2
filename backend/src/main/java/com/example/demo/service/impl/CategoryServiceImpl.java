package com.example.demo.service.impl;

import com.example.demo.domain.Article;
import com.example.demo.domain.Category;
import com.example.demo.rest.ArticleRest;
import com.example.demo.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    public static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    final
    EntityManager em;

    public CategoryServiceImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Article> getArticlesByCategory(String categoryName) {
//        Query q = em.createQuery("select c from Category c where c.")
        return null;
    }

    @Override
    public List<Category> getCategories() {
        Query q = em.createQuery("select c from Category c");
        List<Category> categories = q.getResultList();

        return categories;
    }

    @Override
    public Category getByName(String categoryName) {
        Category category = new Category();

        try {
            Query q = em.createQuery("select c from Category c where c.categoryName = :categoryName");
            q.setParameter("categoryName", categoryName);
            category = (Category) q.getSingleResult();

            if (category == null) {
                category = new Category();
            }
        } catch (NullPointerException e ) {
            logger.error("Please select category!");
        }
        return category;
    }

    @Override
    public Category getById(Long id) {
        Query q = em.createQuery("select c from Category c where c.id = :id");
        q.setParameter("id", id);

        Category category = (Category) q.getSingleResult();

        return category;
    }

    @Override
    public boolean existsByName(String categoryName) {
        Query q = em.createQuery("select c from Category c where c.categoryName = :categoryName");
        q.setParameter("categoryName", categoryName);
        List category = q.getResultList();

        boolean result;

        if(!category.isEmpty()){
            result = true;
        } else {
            result = false;
        }

        return result;
    }

    @Override
    @Transactional
    public void save(Category category) {
        em.persist(category);
        em.flush();
    }

    @Override
    @Transactional
    public void delete(Category category) {
        em.remove(category);
    }
}
