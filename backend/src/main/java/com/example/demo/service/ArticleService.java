package com.example.demo.service;

import com.example.demo.domain.Article;

import java.util.List;

public interface ArticleService {
    List<Article> listAll();
    List<Article> getByAuthor(Long author);

    Article getById(Long id);

    void save(Article article);
    void delete(Article article);
}
