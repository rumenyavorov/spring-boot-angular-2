package com.example.demo.service.impl;

import com.example.demo.domain.Article;
import com.example.demo.domain.User;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    public static final Logger logger = LoggerFactory.getLogger(ArticleServiceImpl.class);


    @Autowired
    private EntityManager em;

    @Override
    public List<Article> listAll() {
        Query q = em.createQuery("select a from Article a");
        List<Article> articles = q.getResultList();

        if(articles == null){
            articles = new ArrayList<>();
        }

        return articles;
    }

    @Override
    @Transactional
    public void save(Article article) {
        em.persist(article);
        em.flush();
    }

    @Override
    @Transactional
    public void delete(Article article) {
        em.remove(article);
    }

   @Override
   public List<Article> getByAuthor(Long author) {
       Query q = em.createQuery("select a from Article a where a.author.id = :author");
       q.setParameter("author", author);
       List<Article> articles = q.getResultList();

       return articles;
   }

    @Override
    public Article getById(Long id) {
        Query q = em.createQuery("select a from Article a where a.id = :id");
        q.setParameter("id", id);
        Article article = null;
        try {
            article = (Article) q.getSingleResult();
        } catch (NoResultException e){
//            e.getMessage();
            logger.error("Article with id: " + id  + " was not found!");
        } catch (NullPointerException e) {
            logger.error("Article doesn't exist!");
        }

        return article;
    }


}
