package com.example.demo.service;

import com.example.demo.domain.Article;
import com.example.demo.domain.Category;

import java.util.List;

public interface CategoryService {
    List<Article> getArticlesByCategory(String categoryName);
    List<Category> getCategories();

    Category getByName(String categoryName);
    Category getById(Long id);

    boolean existsByName(String categoryName);

    void save(Category category);
    void delete(Category category);
}
