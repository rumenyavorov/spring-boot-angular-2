package com.example.demo.service.impl;

import com.example.demo.domain.User;
import com.example.demo.response.ApiResponse;
import com.example.demo.rest.UserRest;
import com.example.demo.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    public static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private EntityManager em;

    @Override
    public List<User> getUsers() {
        Query q = em.createQuery("select u from User u");
        List<User> users = q.getResultList();

        if(users == null){
            users = new ArrayList<>();
        }

        return users;
    }

    @Override
    public User getById(Long id) {
        Query q = em.createQuery("select u from User u where u.id = :id");
        q.setParameter("id", id);
        User user = null;
        try {
            user = (User) q.getSingleResult();
        } catch (NoResultException e){
            logger.info("User not found for this id: {}", id);
        }
        return user;
    }


    @Override
    @Transactional
    public void save(User user) {
        em.persist(user);
        em.flush();
    }

    @Override
    public User getByEmail(String email) {
        Query q = em.createQuery("select u from User u where u.email = :email");
        q.setParameter("email", email);

        User user = new User();

        try {
            user = (User) q.getSingleResult();
        } catch (NoResultException nre){
            logger.info("Email doesn't exist!");
        }
        return user;
    }

    @Override
    @Transactional
    public void delete(User user) {
        em.remove(user);
    }

    @Override
    public List<User> getByName(String name) {
        Query q = em.createQuery("select u from User u where u.name = :name");
        q.setParameter("name", name);

        return q.getResultList();
    }


}
