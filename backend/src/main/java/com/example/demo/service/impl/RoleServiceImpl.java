package com.example.demo.service.impl;

import com.example.demo.domain.Role;
import com.example.demo.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private EntityManager em;

    @Override
    public Role byRoleName(String roleName) {
        Query q = em.createQuery("select r from Role r where r.roleName = :roleName");
        q.setParameter("roleName", roleName);

        return (Role) q.getSingleResult();
    }
}
