package com.example.demo.service;

import com.example.demo.domain.User;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

public interface UserService {
    List<User> getUsers();
    List<User> getByName(String name);

    User getById(Long id);
    User getByEmail(String email);

    void save(User user);
    void delete(User user);
}
