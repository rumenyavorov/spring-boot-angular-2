package com.example.demo.dto;

import java.io.Serializable;

public class CommentSaveDto implements Serializable {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
