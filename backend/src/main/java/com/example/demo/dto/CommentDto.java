package com.example.demo.dto;

import com.example.demo.domain.Article;

import java.io.Serializable;

public class CommentDto implements Serializable {
    private String id;
    private String text;
    private Article article;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
