package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;


@Entity
@Table(name = "article_comments")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "body")
    private String text;

    @ManyToOne(cascade = {CascadeType.PERSIST})
    @JsonIgnore
    private Article article;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnoreProperties(ignoreUnknown = true, value = {"comments", "roles"})
    private User userId;

    public Comment() {
    }

    public Comment(String text, Article article) {
        this.text = text;
        this.article = article;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", article=" + article +
                '}';
    }
}
