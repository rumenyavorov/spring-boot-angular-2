import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UserListComponent } from './components/admin/user-list/user-list.component';
import { UserEditComponent } from './components/admin/user-edit/user-edit.component';
import { CreateArticleComponent } from './components/article/create-article/create-article.component';
import { ArticleEditComponent } from './components/article/article-edit/article-edit.component';
import { HomePageComponent } from './components/home/home-page/home-page.component';
import { CategoriesComponent } from './components/admin/categories/categories.component';
import { CategoryDetailsComponent } from './components/admin/category-details/category-details.component';
import { CommentsComponent } from './components/article/comments/comments.component';
import { AdminGuard } from './core/guards/admin.guard';
import { UsersListResolver } from './core/resolvers/users-list.resolver';
import { ArticleDetailsComponent } from './components/article/article-details/article-details.component';


const appRoutes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'admin/users', component: UserListComponent, canActivate: [AdminGuard], resolve: { users: UsersListResolver}},
  { path: 'admin/edit/:id', component:  UserEditComponent, canActivate: [AdminGuard]},
  { path: 'article/create', component: CreateArticleComponent },
  { path: 'article/details/:id', component: ArticleDetailsComponent },
  { path: 'article/edit/:id', component: ArticleEditComponent },
  { path: 'categories', component: CategoriesComponent, canActivate: [AdminGuard] },
  { path: 'category/details/:id', component: CategoryDetailsComponent },
  { path: 'article/:id/comments', component: CommentsComponent },
  { path: '**', redirectTo: '/not-found' }
];

export const routing = RouterModule.forRoot(appRoutes);
