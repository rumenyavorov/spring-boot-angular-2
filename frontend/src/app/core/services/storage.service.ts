import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  getItem(key: string) {
    return localStorage.getItem(key);
  }

  saveItem(key: string, value: any) {
    return localStorage.setItem(key, JSON.stringify(value));
  }

  deleteItem(key: string) {
    return localStorage.removeItem(key);
  }

  clear() {
    return localStorage.clear();
  }
}
