import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { User } from 'src/app/model/model.user';
import { map } from 'rxjs/operators';
import { StorageService } from './storage.service';


@Injectable()
export class AuthService {
  private userSubject: BehaviorSubject<User> = new BehaviorSubject(this.isUserLogged());
  private isLoggedSubject: BehaviorSubject<boolean> = new BehaviorSubject(this.isLogged());

  constructor(
  private readonly http: HttpClient,
  private readonly router: Router,
  private readonly storageService: StorageService,
  private readonly jwtHelper: JwtHelperService
  ) { }
  authenticated: boolean;

  login({email, password}) {
      return this.http.post('//localhost:8080/login', {email, password})
      .pipe(
        map(
          (data) => {
            console.log(data);
            const user = JSON.parse(this.jwtHelper.decodeToken(data['accessToken']).sub);
            this.storageService.saveItem('token', data['accessToken']);
            console.log(data['accessToken']);
            this.userSubject.next(user);
            this.isLoggedSubject.next(true);
          }
        )
      );
  }

  public logOut() {
    this.storageService.clear();
    this.userSubject.next(null);
    this.isLoggedSubject.next(false);
    this.router.navigateByUrl('/');
  }

  private isLogged() {
    const token = this.storageService.getItem('token');
    if (token) {
      return true;
    }
    return false;
  }

  private isUserLogged() {
    const token = this.storageService.getItem('token');
    if (token !== null) {
      const user = JSON.parse(this.jwtHelper.decodeToken(token).sub);
      return  user;
    }

    return null;
  }

  public get user$() {
    return this.userSubject.asObservable();
  }

  public get isLoggedIn$() {
    return this.isLoggedSubject.asObservable();
  }
}
