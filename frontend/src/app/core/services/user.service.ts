import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { User } from 'src/app/model/model.user';
import { RegisterUser } from 'src/app/model/model.register-user';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public readonly http: HttpClient,
    public readonly authService: AuthService,
    ) { }

  userList(): Observable<User[]> {
    return this.http.get<User[]>('//localhost:8080/admin/users');
  }

  deleteUser(id: string): Observable<User> {
    return this.http.delete<User>('//localhost:8080/admin/users/' + id);
  }

  updateUser({name, email}): Observable<User> {
    const userEditId = this.authService.user$.subscribe((user) => {
      console.log(user.id);
    });

    return this.http.put<User>('//localhost:8080/admin/users/' + userEditId, {name, email});
  }

  userById(id: string): Observable<User> {
    return this.http.get<User>('//localhost:8080/admin/userById?id=' + id);
  }

  loadProfile(): Observable<User> {
    return this.http.get<User>('//localhost:8080/profile');
  }

  createAccount(user: RegisterUser): Observable<User> {
    return this.http.post<User>('//localhost:8080/register', user);
  }
}
