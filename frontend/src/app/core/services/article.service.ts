import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Article} from '../../model/model.article';
import {Observable} from 'rxjs';
import { Category } from 'src/app/model/model.category';
import { Comments } from 'src/app/model/model.comments';
import { SaveComment } from 'src/app/model/model.save-comment';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor(public http: HttpClient) { }

  createArticle(article: Article): Observable<Article> {
    return this.http.post<Article>('//localhost:8080/article', article);
  }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>('//localhost:8080/article');
  }

  articlesByAuthor(id: number): Observable<Article[]> {
    console.log(id);
    return this.http.get<Article[]>('//localhost:8080/article/findArticlesByAuthor?author=' + id);
  }

  articleById(id: number): Observable<Article> {
    return this.http.get<Article>('//localhost:8080/article/' + id);
  }

  deleteArticle(id: number): Observable<Article> {
    return this.http.delete<Article>('//localhost:8080/article/' + id);
  }

  editArticle(id: number, article: Article): Observable<Article> {
    return this.http.put<Article>('//localhost:8080/article/edit/' + id, article);
  }

  createCategory(category: Category): Observable<Category> {
    return this.http.post<Category>('//localhost:8080/category', category);
  }

  getCategories(): Observable<Category[]> {
    return this.http.get<Category[]>('//localhost:8080/category');
  }

  commentsForArticleById(id: number): Observable<Comments[]> {
    return this.http.get<Comments[]>('//localhost:8080/article/' + id + '/comments');
  }

  addCommentForArticle(id: number, comment: SaveComment): Observable<Comments> {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.http.post<Comments>('//localhost:8080/article/' + id + '/comments', comment, {headers: headers});
  }

  deleteComment(articleId: number, commentId: number): Observable<Comments> {
    return this.http.delete<Comments>(`//localhost:8080/article/${articleId}/comments/${commentId}`);
  }
}
