import { NgModule } from '@angular/core';
import { ArticleService } from './services/article.service';
import { JwtModule } from '@auth0/angular-jwt';
import { StorageService } from './services/storage.service';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { UsersListResolver } from './resolvers/users-list.resolver';


@NgModule({
  declarations: [],
  imports: [
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          // console.log(JSON.parse(localStorage.getItem('token')));
          return localStorage.getItem('token');
        },
        whitelistedDomains: ['localhost:8080']
      }
    })
  ],
  providers: [
    UserService,
    ArticleService,
    StorageService,
    AuthService,
    UsersListResolver,
  ]
})
export class CoreModule { }
