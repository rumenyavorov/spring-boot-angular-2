import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';

@Injectable()
export class UsersListResolver implements Resolve<any> {
    constructor(
        private readonly userService: UserService
    ) {}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        return this.userService.userList().pipe(map((data) => {
            if (data) {
                return data;
            }
        }));
    }
}
