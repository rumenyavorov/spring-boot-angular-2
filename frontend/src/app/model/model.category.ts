import { Article } from './model.article';

export class Category {
    id: number;
    categoryName: string;
    articles: Article[];
}
