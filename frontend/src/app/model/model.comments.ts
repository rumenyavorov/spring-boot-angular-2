import { Article } from './model.article';
import { User } from './model.user';

export class Comments {
    id: number;
    text: string;
    article: Article;
    userId: User;
}
