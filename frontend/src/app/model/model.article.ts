import { Comments } from './model.comments';
import { User } from './model.user';
import { Category } from './model.category';

export class Article {
    id: number;
    title: string;
    content: string;
    author: User;
    category: Category;
    comments: Comments[];
}
