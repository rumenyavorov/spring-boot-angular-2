
export class RegisterUser {
    id: number;
    email: string;
    password: string;
    name: string;
    roles: string;
  }