import {Component, OnInit, ViewEncapsulation, ViewChild, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {AuthService} from '../../core/services/auth.service';
import {User} from '../../model/model.user';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Article } from 'src/app/model/model.article';
import { Category } from 'src/app/model/model.category';
import {ArticleService} from '../../core/services/article.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})

export class ProfileComponent implements OnInit, OnDestroy {
  private articlesByAuthorSub: Subscription;
  private userSub: Subscription;

  articlesByAuthor: Article[] = [];
  exists: boolean;
  currentUser: User;
  userRole: any;
  categories: any;
  articlesToLoad: any;

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly articleService: ArticleService,
    private readonly toast: ToastrService
    ) {}

  ngOnInit() {
    this.userSub = this.authService.user$.subscribe((user) => {
      this.currentUser = user;
      try {
        this.articlesByAuthorSub =  this.articleService.articlesByAuthor(this.currentUser.id)
            .subscribe(data => {
              this.articlesByAuthor = data;
            });
      } catch (e) {
        return false;
      }
    });
  }

  ngOnDestroy(): void {
    this.articlesByAuthorSub.unsubscribe();
    this.userSub.unsubscribe();
  }

  // getting article id so its able to load the article details
  loadArticleDetails(id: string) {
    sessionStorage.setItem('articleEditId', id);
    this.router.navigateByUrl('article/details/' + id);
  }

  listCategories() {
    this.articleService.getCategories()
          .subscribe(data => {
            this.categories = data;
            console.log(this.categories);
            sessionStorage.setItem('categories', JSON.stringify(data));
          });
  }

  getByUserFromCategory(category: Category) {
    // const allArticlesFromCategory = this.articlesByAuthor.find(e => e.author.email === category.categoryName).id.map
    const allArticlesFromCategory = this.categories.find(e => e.categoryName === category.categoryName).articleList.map(e => e.id);
    console.log(allArticlesFromCategory);
    console.log(this.categories);
    return this.articlesByAuthor.filter(e => allArticlesFromCategory.includes(e.id));
  }
}
