import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from '../../model/model.user';
import {Router} from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from 'src/app/core/services/user.service';
import { RegisterUser } from 'src/app/model/model.register-user';

@Component( {
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  user: RegisterUser = new RegisterUser();
  errorMessage: string;

  constructor(private readonly userService: UserService,
    public readonly router: Router) {
  }

  ngOnInit() {
  }

  register() {
    this.userService.createAccount(this.user).subscribe(data => {
        this.router.navigate(['/login']);
      }, (err: HttpErrorResponse) => {
      this.errorMessage = err.error['message'];
    }
    );
  }
}
