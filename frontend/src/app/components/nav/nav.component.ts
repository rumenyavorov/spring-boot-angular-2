import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/model/model.user';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  public isLogged: boolean;
  public currentUser: User;
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private toast: ToastrService) {}

  ngOnInit() {
   this.authService.isLoggedIn$.subscribe((isLogged) => {
      this.isLogged = isLogged;
      console.log(this.isLogged);
   });
   this.authService.user$.subscribe((user: User) => {
      this.currentUser = user;
      console.log(user);
   });
  }


  logOut() {
      this.authService.logOut();
      this.router.navigateByUrl('/login');
    }
}
