import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from '../../../model/model.user';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';
import { HttpClientModule, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RegisterUser } from 'src/app/model/model.register-user';
import { StorageService } from 'src/app/core/services/storage.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component( {
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ HttpClientModule]
})
export class LoginComponent implements OnInit {
  user: RegisterUser = new RegisterUser();
  errorMessage: string;
  currentUser: User;
  loginForm: FormGroup;
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [Validators.required],
      password: [Validators.required]
    });
  }

  login() {
    const sub = this.authService.login(this.user).subscribe(
      (_) => {
        this.router.navigateByUrl('/profile');
      }, () => {},
      () => sub.unsubscribe()
    );
  }
}

