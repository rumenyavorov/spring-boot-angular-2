import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { httpFactory } from '@angular/http/src/http_module';
import { ArticleService } from '../../../core/services/article.service';
import { Article } from 'src/app/model/model.article';
import { ToastrService } from 'ngx-toastr';
import { DialogService } from 'src/app/core/services/dialog.service';
import { ArticleEditComponent } from '../../article/article-edit/article-edit.component';
import { Category } from 'src/app/model/model.category';
import { User } from 'src/app/model/model.user';
import { Router } from '@angular/router';
import { ArticleDetailsComponent } from '../../article/article-details/article-details.component';
import { Comments } from 'src/app/model/model.comments';

@Component({
  selector: 'app-home',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  categories: Category[];
  articles: Article[];
  articleDetails: Article;
  articleComments: Number;
  currentUser: User;
  panelOpenState = false;

  constructor(
    private readonly articleService: ArticleService,
    private readonly authService: AuthService,
    private readonly toastr: ToastrService,
    private readonly dialogService: DialogService,
    private readonly router: Router
    ) { }

  ngOnInit() {
    this.loadCategories();
    this.authService.user$.subscribe((data) => {
      this.currentUser = data;
    });
  }

  loadCategories() {
    this.articleService.getCategories()
            .subscribe(categories => {
              console.log(categories);
              this.categories = categories;
              this.articleService.getArticles().subscribe(articles => {
                this.articles = articles;
              });
            });
  }

  onDelete(id: number) {
    this.articleService.deleteArticle(id).subscribe(data => {
      this.loadCategories();
      this.toastr.success('', data['message']);
    });
  }

  editArticle(article: Article) {
    const dialogRef = this.dialogService.open(ArticleEditComponent, article);
    const updateSub = dialogRef.componentInstance.update.subscribe((updatedArticle: Article) => {
      this.articleService.editArticle(updatedArticle.id, updatedArticle).subscribe(() => {
        const articleChangeIndex = this.articles.findIndex(e => e.id === updatedArticle.id);
        this.articles[articleChangeIndex] = updatedArticle;
        dialogRef.close();
      });
    });

    dialogRef.afterClosed().subscribe(() => {
      updateSub.unsubscribe();
    });
  }

  loadArticleDetails(id: number) {
      this.router.navigateByUrl('/article/details/' + id);
  }
}
