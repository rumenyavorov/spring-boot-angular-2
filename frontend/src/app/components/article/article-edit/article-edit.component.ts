import { Component, OnInit, Inject, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Article } from 'src/app/model/model.article';
@Component({
  selector: 'app-article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./article-edit.component.css']
})
export class ArticleEditComponent implements OnInit {
  articleUpdateForm: FormGroup;
  public update: EventEmitter<Article> = new EventEmitter();
  constructor(
    private readonly formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: Article,
    private readonly dialogRef: MatDialogRef<ArticleEditComponent>,
    ) { }

  ngOnInit() {
    this.articleUpdateForm = this.formBuilder.group({
      title: [this.data.title, [Validators.required]],
      content: [this.data.content, [Validators.required]]
    });
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.update.emit({...this.data, ...this.articleUpdateForm.value});
  }
}
