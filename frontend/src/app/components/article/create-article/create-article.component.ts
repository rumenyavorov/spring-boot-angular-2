import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/model/model.article';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {ArticleService} from '../../../core/services/article.service';
import { Category } from 'src/app/model/model.category';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {
  article: Article =  new Article();
  categories: any;
  public selectCategory: Category[];
  constructor(private articleService: ArticleService,
    private router: Router,
    private toastr: ToastrService,
    private authService: AuthService) { }

  ngOnInit() {
    this.getAllCategories();
  }

  getAllCategories() {
    this.articleService.getCategories()
          .subscribe(data => {
            this.categories = data;
            this.categories.forEach(cat => {
              this.selectCategory = cat;
            });
          });
  }

  createArticle() {
    console.log(this.article);
    this.articleService.createArticle(this.article)
            .subscribe(data => {
              console.log(data);
      this.router.navigateByUrl('/profile');
      this.toastr.success('', 'Article created successfully!');
    });
  }
}
