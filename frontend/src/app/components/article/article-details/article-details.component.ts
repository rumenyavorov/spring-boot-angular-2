import { Component, OnInit, Inject, Input, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from 'src/app/model/model.article';
import { ArticleService } from '../../../core/services/article.service';
import { Comments } from 'src/app/model/model.comments';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/model/model.user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-article-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {
    articleDetails: Article;
    articleId: any;
    commentsForArticle: Comments[];
    comment: string;
    currentUser: User;
    constructor(
    private readonly articleService: ArticleService,
    private readonly route: ActivatedRoute,
    private readonly toastr: ToastrService,
    private readonly authService: AuthService
    ) { }
    ngOnInit() {
        this.route.params.subscribe(params => {
            this.articleId = params['id'];
        });
        this.loadArticleDetails();
        this.loadCommentsForArticle();
        this.authService.user$.subscribe((user) => {
            this.currentUser = user;
        });
    }

    loadArticleDetails() {
        this.articleService.articleById(this.articleId).subscribe((articleDetails) => {
            this.articleDetails = articleDetails;
        });
    }

    loadCommentsForArticle() {
        this.articleService.commentsForArticleById(this.articleId).subscribe((comments) => {
            this.commentsForArticle = comments;
        });
    }
    addComment(id: number) {
        this.articleService.addCommentForArticle(id, {text: this.comment}).subscribe((data) => {
            this.commentsForArticle.push(data);
            console.log(this.comment);
            this.loadCommentsForArticle();
            this.comment = '';
        });
    }
    deleteComment(articleId: number, commentId: number) {
        this.articleService.deleteComment(articleId, commentId).subscribe((data) => {
            this.loadCommentsForArticle();
            this.toastr.success('', data['message']);
        });
    }
}
