import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material';
import { UserService } from '../../../core/services/user.service';
import { User } from 'src/app/model/model.user';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[];
  role: string;
  displayedColumns: string[] = ['id', 'name', 'email', 'role'];
  dataSource: any;
  constructor(
    private authService: AuthService, 
    private router: Router, 
    private userService: UserService, 
    private toast: ToastrService,
    private readonly route: ActivatedRoute
    ) {}

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.users = data.users;
      this.dataSource = new MatTableDataSource(this.users);
    })
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id)
        .subscribe(
          _ => {
            this.toast.success('', 'User deleted!');
            this.toast.info('', 'Articles by this user deleted!');
          },
          error => console.log(error));
  }

  updateUser(id: string) {
    localStorage.setItem('userEditId', id);
    this.router.navigateByUrl('/admin/edit/' + id);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
