import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/model/model.category';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from 'src/app/core/services/user.service';
import { ArticleService } from 'src/app/core/services/article.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  category: Category = new Category();
  errorMessage: string;

  constructor(
    private readonly articleService: ArticleService,
    private readonly toast: ToastrService,
    private readonly router: Router
    ) { }

  ngOnInit() {
  }

  createCategory() {
    this.articleService.createCategory(this.category)
            .subscribe(_ => {
              this.toast.success('', 'Category added successfully!');
              this.router.navigateByUrl('/');
            }, (err: HttpErrorResponse) => {
              this.errorMessage = err.error['message'];
            });
  }
}
