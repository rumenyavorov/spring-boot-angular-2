import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/model.user';
import { Router } from '@angular/router';
import { stringify } from 'querystring';
import { NgForm, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  public userData;
  constructor(private authService: AuthService, private userService: UserService,
    private router: Router, private toast: ToastrService) { }

  ngOnInit() {
    this.loadUserToEdit();
  }

  // checkAuth() {
  //   const checkRole = JSON.parse(localStorage.getItem('currentUser'));
  //   if ((checkRole.roles[0].roleName !== 'ADMIN')) {
  //       this.router.navigateByUrl('/not-found');
  //   }
  // }

  userById(id: string) {
    const user = this.userService.userById(id);

    return user;
  }

  loadUserToEdit() {
    const userEditId = localStorage.getItem('userEditId');
    const user = this.userById(userEditId);

    return user.subscribe(data => { this.userData = data;
    });
  }

  updateUser() {
    const name = ((document.getElementById('name') as HTMLInputElement).value);
    const email = ((document.getElementById('email') as HTMLInputElement).value);
    this.userService.updateUser({name, email})
      .subscribe(updateData => {
        console.log(updateData);
      });

      setTimeout(() => {
        this.router.navigateByUrl('/admin/users');
        this.toast.success('','User edited successfully!');
      },
      500);
  }
}
