import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FormsModule } from '@angular/forms';
import { ProfileComponent } from './components/profile/profile.component';
import {routing} from './app.routing';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavComponent } from './components/nav/nav.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { UserListComponent } from './components/admin/user-list/user-list.component';
import { UserEditComponent } from './components/admin/user-edit/user-edit.component';
import { CreateArticleComponent } from './components/article/create-article/create-article.component';
import { ArticleDetailsComponent } from './components/article/article-details/article-details.component';
import { ArticleEditComponent } from './components/article/article-edit/article-edit.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { JwtinterceptorService } from './services/jwtinterceptor.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { CategoriesComponent } from './components/admin/categories/categories.component';
import { CategoryDetailsComponent } from './components/admin/category-details/category-details.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatFormFieldModule, MatFormFieldControl } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app/modules/material.module';
import {MatExpansionModule} from '@angular/material/expansion';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { FileUploadModule } from 'ng2-file-upload';
import { CommentsComponent } from './components/article/comments/comments.component';
import { HomePageComponent } from './components/home/home-page/home-page.component';
import { CoreModule } from './core/core.module';
import { AuthService } from './core/services/auth.service';
import { JwtModule, JWT_OPTIONS, JwtHelperService } from '@auth0/angular-jwt';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    RegisterComponent,
    NotFoundComponent,
    NavComponent,
    UserListComponent,
    UserEditComponent,
    CreateArticleComponent,
    ArticleDetailsComponent,
    ArticleEditComponent,
    HomePageComponent,
    CategoriesComponent,
    CategoryDetailsComponent,
    CommentsComponent,
    HomePageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpClientModule,
    ReactiveFormsModule,
    MDBBootstrapModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-bottom-right'
    }),
    BrowserAnimationsModule,
    NgxPaginationModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MaterialModule,
    MatFormFieldModule,
    MatExpansionModule,
    ScrollingModule,
    FileUploadModule,
    CoreModule
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtinterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
  ]
})
export class AppModule { }
